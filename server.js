const express = require('express');
const http = require('http');
const path = require('path');
const io = require('socket.io');

const app = express();
const server = http.createServer(app);
const ws = new io.Server(server);

app.get('/', (req, res) => {
  res.sendFile(path.resolve('index.html'));
});

const users = {};

ws.on('connection', (socket) => {
  console.log('Client connected!');

  socket.on('chat message', (msg) => {
    socket.broadcast.emit('new message', msg);
  });

  socket.on('user joined', (username) => {
    socket.broadcast.emit('new user', username);
    users[socket.id] = username;
  });

  socket.on('disconnecting', () => {
    const username = users[socket.id];
    socket.broadcast.emit('left chat', username);
  });
});

server.listen(8080, () => {
  console.log('Server is running on port 8080');
});
